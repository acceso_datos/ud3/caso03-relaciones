/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo.modeloNN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity(name="PersonajeNN")
@Table(name = "personajes", catalog = "bdrelacionesNN")
public class Personaje implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "nombre", nullable = true, length = 20)
	private String nombre;

	// Determinamos que la clase Personajes es la propietaria de la relación
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(catalog = "bdrelacionesNN", name = "personaje_tiene_armas", 
	 joinColumns = @JoinColumn(name = "id_personaje"), 
	 inverseJoinColumns = @JoinColumn(name = "id_arma"))
	List<Arma> armas = new ArrayList<Arma>(0);

	public Personaje() {
	}

	public Personaje(String nombre) {
		this.nombre = nombre;
	}

	public Personaje(String nombre, List<Arma> armas) {
		this.nombre = nombre;
		this.armas = armas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Arma> getArmas() {
		return armas;
	}

	public void setArmas(List<Arma> armas) {
		this.armas = armas;
	}

	public void addArma(Arma arma) {
		armas.add(arma);
	}

	public void removeArma(Arma arma) {
		armas.remove(arma);
	}

	@Override
	public String toString() {
		return "Personaje [id=" + id + ", nombre=" + nombre + ", numArmas=" + armas.size() + "]";
	}

}
