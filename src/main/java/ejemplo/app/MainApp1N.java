package ejemplo.app;

import ejemplo.modelo1N.Arma;
import ejemplo.modelo1N.Personaje;
import jakarta.persistence.EntityManager;

public class MainApp1N {

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - ARMAS DE PERSONAJES - RELACION 1-N");

		try {
			EntityManager orm = ConexionEM.getEntityManager("armapersonaje1N");

			// Creamos arma y la guardamos en la bd
			Arma arma = new Arma("Espada");
			orm.getTransaction().begin();
			orm.persist(arma);			
			orm.getTransaction().commit();
			System.out.println("\tInsertada arma " + arma);

			// Creamos personajes y les asociamos arma
			// Esto hará que automáticamente el arma tenga
			// asociados los personajes que la poseen
			Personaje p1 = new Personaje("persona1", arma);
			Personaje p2 = new Personaje("persona2");
			p2.setArma(arma);
			orm.getTransaction().begin();
			orm.persist(p1);
			orm.persist(p2);
			orm.getTransaction().commit();
			System.out.println("\tInsertados personajes " + p1 + " y " + p2);

			System.out.println("------------------------");

			// Podemos recorrer los personajes de un arma concreta
			orm.refresh(arma);
			System.out.println("\tPersonajes con la arma " + arma);
			for (Personaje pers : arma.getPersonajes()) {
				System.out.println("\t\t" + pers);
			}

			// Podemos añadir un personaje más al arma
			Personaje p3 = new Personaje("persona3");
			orm.getTransaction().begin();
			orm.persist(p3);
			arma.addPersonaje(p3);
			System.out.println("Añadido personaje " + p3 + " a la arma " + arma);
			orm.getTransaction().commit();
			
			ConexionEM.closeEntityManager();
			
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
