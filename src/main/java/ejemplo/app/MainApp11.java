package ejemplo.app;

import ejemplo.modelo11.Arma;
import ejemplo.modelo11.Personaje;
import jakarta.persistence.EntityManager;

public class MainApp11 {

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - ARMAS DE PERSONAJES - RELACION 1-1");

		try {
			EntityManager  orm = ConexionEM.getEntityManager("armapersonaje11");

			// Creamos arma y la guardamos en la bd
			Arma armaEspada = new Arma("Espada");
			orm.getTransaction().begin();
			orm.persist(armaEspada);
			orm.getTransaction().commit();
			System.out.println("\tInsertada arma " + armaEspada);

			Arma armaEscudo = new Arma("Escudo");
			orm.getTransaction().begin();
			orm.persist(armaEscudo);
			orm.getTransaction().commit();
			System.out.println("\tInsertada arma " + armaEscudo);

			// Creamos personajes y les asociamos arma
			// Esto hará que automáticamente el arma tenga
			// asociados los personajes que la poseen
			Personaje p1 = new Personaje("persona1", armaEspada);
			Personaje p2 = new Personaje("persona2", armaEscudo);
			orm.getTransaction().begin();
			orm.persist(p1);
			orm.persist(p2);
			orm.getTransaction().commit();
			System.out.println("\tInsertados personajes " + p1 + " y " + p2);

			System.out.println("--------------------");
			
			//Conexion.closeSession();
			//Session sesion2 = Conexion.getSession();
			// Podemos ver el personaje asociado a cada arma
			orm.refresh(armaEspada);			
			System.out.print("\t\t" + armaEspada);
			if (armaEspada.getPersonaje() != null) {
				System.out.println(" - Está asignada a un personaje y es: " + armaEspada.getPersonaje().getNombre());
			}

			orm.refresh(armaEscudo);
			System.out.print("\t\t" + armaEscudo);
			if (armaEscudo.getPersonaje() != null) {
				System.out.println(" - Está asignada a un personaje y es: " + armaEscudo.getPersonaje().getNombre());
			}
			ConexionEM.closeEntityManager();

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
