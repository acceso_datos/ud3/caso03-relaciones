package ejemplo.app;

import ejemplo.modeloNN.Arma;
import ejemplo.modeloNN.Personaje;
import jakarta.persistence.EntityManager;

public class MainAppNN {

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - ARMAS DE PERSONAJES - RELACION N-N");

		try {
			EntityManager orm = ConexionEM.getEntityManager("armapersonajeNN");

			// Creamos arma y la guardamos en la bd
			Arma armaEspada = new Arma("Espada");
			Arma armaEscudo = new Arma("Escudo");
			orm.getTransaction().begin();
			orm.persist(armaEspada);
			orm.persist(armaEscudo);
			orm.getTransaction().commit();
			System.out.println("\tInsertadas armas: " + armaEspada + " y " + armaEscudo);

			// Creamos personajes y los guardamos en la bd
			Personaje p1 = new Personaje("persona1");
			Personaje p2 = new Personaje("persona2");
			orm.getTransaction().begin();
			orm.persist(p1);
			orm.persist(p2);
			orm.getTransaction().commit();
			System.out.println("\tInsertados personajes " + p1 + " y " + p2);

			
			// Añadimos armas a los personajes. Lo hacemos a través
			// de personajes porque es Personaje la propietaria de la relacion.
			orm.getTransaction().begin();;
			p1.addArma(armaEspada);
			p1.addArma(armaEscudo);
			p2.addArma(armaEspada);
			orm.getTransaction().commit();
			
			// Consultamos las armas de un personajes concreto
			System.out.println("\tEl personaje " + p1.getId() + " tienes estas armas: ");
			for(Arma arma: p1.getArmas()) {
				System.out.println("\t\t" + arma);
			}
			
			// Consultamos los personajes que tiene una arma en concreto
			orm.refresh(armaEspada);
			System.out.println("\tLa arma " + armaEspada.getId() + " la poseen estos personajes: ");
			for(Personaje pers: armaEspada.getPersonajes()) {
				System.out.println("\t\t" + pers);
			}		

			ConexionEM.closeEntityManager();
			
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
