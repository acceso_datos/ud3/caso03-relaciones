package ejemplo.app;

import ejemplo.modelo2NN.Arma;
import ejemplo.modelo2NN.Personaje;
import ejemplo.modelo2NN.PersonajeTieneArma;
import jakarta.persistence.EntityManager;

public class MainApp2NN {

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - ARMAS DE PERSONAJES - RELACION N-N con atributos");

		try {
			EntityManager orm = ConexionEM.getEntityManager("armapersonaje2NN");

			// Creamos arma y la guardamos en la bd
			Arma armaEspada = new Arma("Espada");
			Arma armaEscudo = new Arma("Escudo");
			orm.getTransaction().begin();
			orm.persist(armaEspada);
			orm.persist(armaEscudo);
			orm.getTransaction().commit();
			System.out.println("\tInsertadas armas: " + armaEspada + " y " + armaEscudo);

			// Creamos personajes y los guardamos en la bd
			Personaje p1 = new Personaje("persona1");
			Personaje p2 = new Personaje("persona2");
			orm.getTransaction().begin();
			orm.persist(p1);
			orm.persist(p2);
			orm.getTransaction().commit();
			System.out.println("\tInsertados personajes " + p1 + " y " + p2);

			
			// Añadimos armas a los personajes. Lo hacemos a través
			// de personajes porque es Personaje la propietaria de la relacion.
			orm.getTransaction().begin();			
			PersonajeTieneArma pta1 = new PersonajeTieneArma(armaEscudo, p1, 8);
			orm.persist(pta1);	
			PersonajeTieneArma pta2 = new PersonajeTieneArma(armaEspada, p1, 7);
			orm.persist(pta2);
			PersonajeTieneArma pta3 = new PersonajeTieneArma(armaEscudo, p2, 6);
			orm.persist(pta3);
			orm.getTransaction().commit();
			
			// Consultamos las armas de un personajes concreto
			System.out.println("\tEl personaje " + p1.getId() + " tienes estas armas: ");
			for(PersonajeTieneArma pta: p1.getPersonajeTieneArmas()) {
				System.out.println("\t\t" + pta.getArma() + " con poder " + pta.getPoder());
			}
			
			// Consultamos los personajes que tiene una arma en concreto
			orm.refresh(armaEspada);
			System.out.println("\tLa arma " + armaEspada.getId() + " la poseen estos personajes: ");
			for(PersonajeTieneArma pta: armaEspada.getPersonajeTieneArmas()) {
				System.out.println("\t\t" + pta.getPersonaje() + " con poder " + pta.getPoder());
			}		

			ConexionEM.closeEntityManager();
			
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
