/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo.modelo1N;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity(name="Arma1N")
@Table(name = "armas", catalog = "bdrelaciones1N")
public class Arma implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "nombre", nullable = false, length = 30)
    private String nombre;
    
    @OneToMany(mappedBy = "arma", fetch = FetchType.LAZY)
    private List<Personaje> personajes = new ArrayList<Personaje>(0);

    public Arma() {
    }

    public Arma(String nombre) {
        this.nombre = nombre;
    }

    public Arma(String nombre, List<Personaje> personajes) {
        this.nombre = nombre;
        this.personajes = personajes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Personaje> getPersonajes() {
        return personajes;
    }

    public void setPersonajes(List<Personaje> personajes) {
        this.personajes = personajes;
    }

    public void addPersonaje(Personaje personaje) {
        personajes.add(personaje);
        personaje.setArma(this);
    }

    public void removePersonaje(Personaje personaje) {
        personajes.remove(personaje);
        personaje.setArma(null);
    }

	@Override
	public String toString() {
		return "Arma [id=" + id + ", nombre=" + nombre + "]";
	} 
}
