/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo.modelo11;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity(name="Arma11")
@Table(name = "armas", catalog = "bdrelaciones11")
public class Arma implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "nombre", nullable = false, length = 30)
    private String nombre;
    
    @OneToOne(mappedBy = "arma")
    private Personaje personaje;

    public Arma() {
    }

    public Arma(String nombre) {
        this.nombre = nombre;
    }

    public Arma(String nombre, Personaje personaje) {
        this.nombre = nombre;
        this.personaje = personaje;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Personaje getPersonaje() {
        return personaje;
    }

    public void setPersonaje(Personaje personaje) {
        this.personaje = personaje;
    }

	@Override
	public String toString() {
		return "Arma [id=" + id + ", nombre=" + nombre + "]";
	}

	
}
