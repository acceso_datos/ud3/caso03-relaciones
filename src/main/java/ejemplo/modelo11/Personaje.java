/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo.modelo11;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity(name="Personaje11")
@Table(name = "personajes", catalog = "bdrelaciones11")
public class Personaje implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "nombre", nullable = true, length = 20)
    private String nombre;
	
	@OneToOne
	@JoinColumn(name = "armas_id", unique = true, nullable = true)
    private Arma arma;

    public Personaje() {
    }
    
    public Personaje(String nombre) {
        this.nombre = nombre;
    }

    public Personaje(String nombre, Arma arma) {
        this.nombre = nombre;
        this.arma = arma;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

	@Override
	public String toString() {
		return "Personaje [id=" + id + ", nombre=" + nombre + ", arma=" + arma.getNombre() + "]";
	}

    
}
